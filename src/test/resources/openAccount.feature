Feature: Open customer in system

  I want to open account customer in system

  Scenario Outline: Open account customer successfully
    Given I open the open account website<website>
    Then I verify that the correct Open Customer form
    When I select a customer name <firstname> <lastname>, select a currency <currency>
    And I click process button
    Then I see successfully dialog is appear
    And I verify the customer information inserts successfully

    Examples:
      | website                                                                            | firstname | lastname | currency |
      | http://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager/openAccount | le        | khoa     | Pound    |


