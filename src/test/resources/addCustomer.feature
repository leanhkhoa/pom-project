Feature: Add customer in system
  in order to access the system
  I want to have the adding customer is system

  Scenario Outline: Add customer successfully
    Given I open the website<website>
    Then I verify that the correct Add Customer form
    When I type first name as <firstname>, lastname as <lastname>, postcode as <postcode>
    And I click Add Customer button
    Then I see the  successfully dialog is appear
    And I verify the open account customer information inserts successfully

    Examples:
      | website                                                                        | firstname | lastname | postcode |
      | http://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager/addCust | le        | khoa     | 123      |
      | http://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager/addCust | vo        | ha       | 457      |