package modules;


import org.junit.Assert;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.Select;

import pages.OpenAccountPage;
import steps.SetUp;

import java.util.HashMap;
import java.util.List;

public class OpenAction {

    public static void clickOpenbtn() throws InterruptedException {
        OpenAccountPage.btnopen.click();
        Thread.sleep(3000);


    }

    public static void verifyAccount(WebDriver driver, String firstname, String lastname) throws InterruptedException {

        WebElement select = driver.findElement(By.id("userSelect"));
        List<WebElement> listOption = select.findElements(By.tagName("option"));
        Boolean checkExisted = false;
        for (WebElement e : listOption) {
            if (e.getText().equals(firstname + " " + lastname)) {
                checkExisted = true;
            }
        }
        Assert.assertEquals(checkExisted, true);
    }
    public static void selectcutomer (WebDriver driver,HashMap<String, String> map) throws Throwable{
        Select selectcustomername= new Select(SetUp.driver.findElement(By.id("userSelect")));
        selectcustomername.selectByVisibleText(map.get("customername1"));
        Thread.sleep(2000);
        Select selectcurrency= new Select(SetUp.driver.findElement(By.id("currency")));
        selectcurrency.selectByVisibleText(map.get("currency1"));
        Thread.sleep(2000);



    }
    public static void clickprcessbtn(WebDriver driver){
        OpenAccountPage.btnprocess.click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }

    }











