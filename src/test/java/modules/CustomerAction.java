package modules;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.CustomerPage;

import java.util.List;

public class CustomerAction {
    public static void customerclick() throws InterruptedException {
        CustomerPage.btncustomer.click();
        Thread.sleep(3000);
    }

    public static void verifyCustomermanager(WebDriver driver, String firstname, String lastname) {
        WebElement table = driver.findElements(By.tagName("table")).get(0);
        WebElement tbodytable = table.findElement(By.tagName("tbody"));
        List<WebElement> trlistt = tbodytable.findElements(By.tagName("tr"));
        Boolean checkExisted = false;
        for (WebElement tr : trlistt) {
            //lay dong dau tien va xoa dong dau tien do
            //List<WebElement> tdlistt = trlistt.get(0).findElements(By.tagName("td"));
            List<WebElement> tdlistt = tr.findElements(By.tagName("td"));
            if (tdlistt != null && tdlistt.size() > 0) {
                String firstn = tdlistt.get(0).getText();
                System.out.println(firstn);
                System.out.println(firstname);
                if (firstn.toLowerCase().equals(firstname.toLowerCase())) {
                    checkExisted = true;
                }
            }
        }
        Assert.assertEquals(checkExisted, true);
    }
}

