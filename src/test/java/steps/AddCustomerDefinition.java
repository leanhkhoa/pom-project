package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import modules.AddCustomerAction;
import modules.OpenAction;
import org.openqa.selenium.support.PageFactory;
import pages.AddCustomerPage;
import pages.OpenAccountPage;

import java.util.HashMap;


public class AddCustomerDefinition {
    private String firstnamestore = "";
    private String lastnamestore = "";
    private String postcodestore = "";

    @Given("^I open the website(.+)$")
    public void i_open_the_website(String website) throws Throwable {
        SetUp.setupDriver();
        SetUp.driver.get(website);
        Thread.sleep(2000);

    }

    @When("^I type first name as (.+), lastname as (.+), postcode as (.+)$")
    public void i_type_first_name_as_lastname_as_postcode_as(String firstname, String lastname, String postcode) throws Throwable {
        PageFactory.initElements(SetUp.driver, AddCustomerPage.class); // PageFactory lay element
        HashMap<String, String> samleDate = new HashMap<String, String>();
        samleDate.put("fname", firstname);
        samleDate.put("lname", lastname);
        samleDate.put("pcode", postcode);

        AddCustomerAction.execute(SetUp.driver, samleDate);

        firstnamestore = firstname;
        lastnamestore = lastname;
        postcodestore = postcode;


    }

    @Then("^I verify that the correct Add Customer form$")
    public void i_verify_that_the_correct_add_customer_form() throws Throwable {
      AddCustomerAction.verifyfrom();

    }


    @Then("^I see the  successfully dialog is appear$")
    public void i_see_the_successfully_dialog_is_appear() throws Throwable {


    }

    @And("^I click Add Customer button$")
    public void i_click_add_customer_button() throws Throwable {
        AddCustomerAction.addclickcustomer(SetUp.driver);



    }
    @And("^I verify the open account customer information inserts successfully$")
    public void i_verify_the_open_account_customer_information_inserts_successfully() throws Throwable {
        PageFactory.initElements(SetUp.driver, OpenAccountPage.class);
        OpenAction.clickOpenbtn();
        Thread.sleep(3000);
        OpenAction.verifyAccount(SetUp.driver,firstnamestore,lastnamestore);
    }

}

