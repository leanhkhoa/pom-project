package steps;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import modules.CustomerAction;
import modules.OpenAction;
import org.openqa.selenium.support.PageFactory;
import pages.CustomerPage;
import pages.OpenAccountPage;

import java.util.HashMap;

public class OpenAccountDefinition {
    private String firstnamestore = "";
    private String lastnamestore = "";

    @Given("^I open the open account website(.+)$")
    public void i_open_the_open_account_website(String website) throws Throwable {
        SetUp.setupDriver();
        SetUp.driver.get(website);
        Thread.sleep(2000);
    }

    @When("^I select a customer name (.+) (.+), select a currency (.+)$")
    public void i_select_a_customer_name_select_a_currency(String fisrtname,String lastname, String currency) throws Throwable {
        PageFactory.initElements(SetUp.driver, OpenAccountPage.class); // PageFactory lay element
        HashMap<String, String> samleDate = new HashMap<String, String>();
        samleDate.put("customername1", fisrtname+" "+lastname);
        samleDate.put("currency1",currency);
        OpenAction.selectcutomer(SetUp.driver,samleDate );
        firstnamestore = fisrtname;
        lastnamestore = lastname;

    }

    @Then("^I verify that the correct Open Customer form$")
    public void i_verify_that_the_correct_open_customer_form() throws Throwable {

    }

    @Then("^I see successfully dialog is appear$")
    public void i_see_successfully_dialog_is_appear() throws Throwable {



    }

    @And("^I click process button$")
    public void i_click_process_button() throws Throwable {

        OpenAction.clickprcessbtn(SetUp.driver);

    }
    @And("^I verify the customer information inserts successfully$")
    public void i_verify_the_customer_information_inserts_successfully() throws Throwable {
        PageFactory.initElements(SetUp.driver, CustomerPage.class);
        CustomerAction.customerclick();
        Thread.sleep(2000);
        CustomerAction.verifyCustomermanager(SetUp.driver,firstnamestore,lastnamestore);

    }



}
