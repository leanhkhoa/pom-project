package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CustomerPage extends BaseClass {
    public CustomerPage (WebDriver driver){
        super(driver);}
    @FindBy(how= How.XPATH,using = "//div[1]/div/div[2]/div/div[1]/button[3]")
    public static WebElement btncustomer;
}
