package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OpenAccountPage extends BaseClass {
    public OpenAccountPage(WebDriver driver){
        super(driver);}
    @FindBy(how= How.XPATH,using = "//div/div[2]/div/div[1]/button[2]")
    public static WebElement btnopen;

    @FindBy(how= How.ID,using = "userSelect")
    public static WebElement customename;
    @FindBy(how= How.ID,using = "currency")
    public static WebElement currency;
    @FindBy(how= How.XPATH,using = "//div/div[2]/div/div[2]/div/div/form/button")
    public static WebElement btnprocess;


}


